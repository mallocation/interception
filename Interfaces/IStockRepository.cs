﻿namespace Interfaces
{
    public interface IStockRepository
    {
        decimal LoadTradingPrice(string stockSymbol);
    }
}