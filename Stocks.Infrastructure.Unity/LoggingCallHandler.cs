﻿using System;
using System.Diagnostics;
using System.Text;
using Unity.Interception.PolicyInjection.Pipeline;

namespace Stocks.Infrastructure.Unity
{
    public class LoggingCallHandler : ICallHandler
    {
        public int Order { get; set; }

        public IMethodReturn Invoke(IMethodInvocation input, GetNextHandlerDelegate getNext)
        {
            var logEntry = new StringBuilder();

            logEntry.Append(DateTimeOffset.Now);
            logEntry.AppendFormat(" Method: {0}.{1}", input.Target.GetType().FullName, input.MethodBase.Name);

            if (input.Arguments?.Count > 0)
            {
                logEntry.Append(" Arguments:");

                foreach (var arg in input.Arguments)
                {
                    logEntry.Append($" {arg}");
                }
            }

            Trace.WriteLine(logEntry.ToString());

            return getNext()(input, getNext);
        }
    }
}