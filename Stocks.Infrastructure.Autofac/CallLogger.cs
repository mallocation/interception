﻿using Castle.DynamicProxy;
using System;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Stocks.Infrastructure.Autofac
{
    public class CallLogger : IInterceptor
    {
        public void Intercept(IInvocation invocation)
        {
            var logEntry = new StringBuilder();

            logEntry.Append(DateTimeOffset.Now);
            logEntry.AppendFormat(" Method: {0}.{1}", invocation.TargetType.FullName, invocation.MethodInvocationTarget.Name);

            if (invocation.Arguments?.Length > 0)
            {
                logEntry.Append(" Arguments: " + string.Join(", ", invocation.Arguments.Select(a => $"{a}")));
            }

            Trace.WriteLine(logEntry.ToString());

            invocation.Proceed();
        }
    }
}