﻿using Interfaces;
using System;
using System.Windows.Forms;

namespace StocksClient
{
    public partial class Form1 : Form
    {
        private readonly IStockRepository stocksRepository;

        public Form1(IStockRepository stocksRepository)
        {
            InitializeComponent();
            this.stocksRepository = stocksRepository;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var price = stocksRepository.LoadTradingPrice(textBox1.Text);

            MessageBox.Show($"Stock Price: {price:C}");
        }
    }
}