﻿using Interfaces;
using Services;
using Stocks.Infrastructure.Unity;
using System;
using System.Windows.Forms;
using Unity;
using Unity.Injection;
using Unity.Interception;
using Unity.Interception.ContainerIntegration;
using Unity.Interception.Interceptors.InstanceInterceptors.InterfaceInterception;
using Unity.Interception.PolicyInjection;
using Unity.Interception.PolicyInjection.MatchingRules;

namespace StocksClient
{
    internal static class UnityProgram
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var container = BootstrapUnityContainer();
            Application.Run(container.Resolve<Form1>());
        }

        private static IUnityContainer BootstrapUnityContainer()
        {
            var container = new UnityContainer();

            container.AddNewExtension<Interception>();

            container.Configure<Interception>()
                .AddPolicy("logging")
                .AddMatchingRule<AssemblyMatchingRule>(new InjectionConstructor("Services"))
                .AddCallHandler<LoggingCallHandler>();

            container.RegisterType<IStockRepository, StockRepository>(
                new InterceptionBehavior<PolicyInjectionBehavior>(),
                new Interceptor<InterfaceInterceptor>());

            // Or, standardize for all repository classes
            //container.RegisterRepository<IStockRepository, StockRepository>();

            container.RegisterType<Form1>();

            return container;
        }

        private static void RegisterRepository<TFrom, TTo>(this IUnityContainer container) where TTo : TFrom
        {
            container.RegisterType<TFrom, TTo>(
                new InterceptionBehavior<PolicyInjectionBehavior>(),
                new Interceptor<InterfaceInterceptor>());
        }
    }
}