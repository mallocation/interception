﻿using Autofac;
using Autofac.Extras.DynamicProxy;
using Interfaces;
using Services;
using Stocks.Infrastructure.Autofac;
using System;
using System.Windows.Forms;
using Unity;

namespace StocksClient
{
    internal static class AutofacProgram
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var container = BoostrapAutofacContainer();

            Application.Run(container.Resolve<Form1>());
        }

        private static IContainer BoostrapAutofacContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<CallLogger>();

            builder.RegisterType<StockRepository>()
                .As<IStockRepository>()
                .EnableInterfaceInterceptors()
                .InterceptedBy(typeof(CallLogger));

            builder.RegisterType<Form1>();

            return builder.Build();
        }
    }
}