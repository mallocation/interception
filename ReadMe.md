## Running the Project
1. Restore NuGet Packages
1. Right-click `StocksClient`, and click `Set as StartUp Project`.
1. To observe the logging output, open the `Output` window, and set "Show output from:" to `Debug`.
1. Click `Start`.

### To switch between Autofac (default) and Unity:
1. Open the Properties for `StocksClient`.
1. Under `Startup object:`, choose between Autofac or Unity.